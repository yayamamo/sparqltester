package jp.ac.rois.dbcls;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import org.topbraid.spin.arq.ARQ2SPIN;
import org.topbraid.spin.arq.ARQFactory;
import org.topbraid.spin.system.SPINModuleRegistry;
import org.topbraid.spin.vocabulary.SP;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryParseException;
import com.hp.hpl.jena.rdf.model.AnonId;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.FileUtils;
import com.hp.hpl.jena.vocabulary.RDF;

public class MakeSpin {

	private static String graphname;
	static HashSet<Resource> hs = new HashSet<Resource>(Arrays.asList(
			SP.Ask, SP.Construct, SP.Describe, SP.Select,
			SP.Clear, SP.Create, SP.DeleteData, SP.DeleteWhere, SP.Drop, SP.InsertData, SP.Load, SP.Modify
			));

	private static boolean validateQuery(String q){
		SPINModuleRegistry.get().init();
		try{
			ARQFactory.get().createQuery(q);
		}catch(QueryParseException e){
			String em = e.getMessage();
			if(em.startsWith("Encountered")){
				try {
					ARQFactory.get().createUpdateRequest(q);
				} catch (QueryParseException eu){
					return false;
				}
			} else {
				return false;
			}
		}catch(Exception e){
			return false;
		}
		return true;
	}

	private static Resource convertQuery(String query){
		SPINModuleRegistry.get().init();
		Model model = ModelFactory.createDefaultModel();
		Property sp_text = model.createProperty("http://spinrdf.org/sp#text");
		try {
			Query arqQuery = ARQFactory.get().createQuery(query);
			//System.out.println(arqQuery.getQueryPattern().toString());
			//AlgebraGenerator ag = new AlgebraGenerator();
			//ag.compile(arqQuery).output(IndentedWriter.stdout);
			ARQ2SPIN arq2SPIN = new ARQ2SPIN(model);
			arq2SPIN.createQuery(arqQuery, null);
		} catch (QueryParseException e) {
			String em = e.getMessage();
			if(em.startsWith("Encountered")){
				try {
					List<com.hp.hpl.jena.update.Update> operations = ARQFactory.get().createUpdateRequest(query).getOperations();
					for(com.hp.hpl.jena.update.Update operation : operations) {
						ARQ2SPIN sparql2SPIN = new ARQ2SPIN(model);
						sparql2SPIN.createUpdate(operation, null);
					}
				} catch (QueryParseException eu){
					System.err.println("Error [" + graphname + "]:" + eu.getMessage());
				}
			} else {
				System.err.println("Error [" + graphname + "]:" + em);
			}
		}
		//model.write(System.out, FileUtils.langTurtle);
		Resource rootSbj = null;
		StmtIterator st = model.listStatements(null, RDF.type, (Resource)null);
		for(;st.hasNext();){
			Statement cs = st.nextStatement();
			if(hs.contains(cs.getObject())){
				rootSbj = cs.getSubject();
				model.add(rootSbj, sp_text, model.createTypedLiteral(query));
			}
		}
		if(rootSbj == null)
			model.add(model.createResource(), sp_text, model.createTypedLiteral(query));
		StringWriter sw = new StringWriter();
		model.write(sw, FileUtils.langNTriple);
		model.close();
		for(String t: sw.toString().split("\\n")){
			System.out.println(t.replaceFirst("\\.$", "") + graphname + " .");
		}
		return rootSbj;
	}

	public static void main(String[] args) {
		Pattern pat = Pattern.compile("^(?:prefix|base)", Pattern.CASE_INSENSITIVE); 
		ClassLoader.getSystemResource("log4j.properties");
		SPINModuleRegistry.get().init();
		BufferedReader br;
		ArrayList<Resource> rsclist = new ArrayList<Resource>();
		try {
			File f = new File(args[0]);
			graphname = "<http://uri.sparqltest.org/graph99/" + URLEncoder.encode(f.getName(), "UTF-8") + ">";
			br = new BufferedReader(new FileReader(f));
			String query = "";
			String s;
			String prologue = "";
			while((s = br.readLine()) != null){
				s = s.trim();
				if(s.startsWith("#"))
					continue;
				if(s.matches("^\\s*$"))
					continue;
				if(pat.matcher(s).find()){
					prologue += s + '\n';
					continue;
				}
				if(s.endsWith(";") && validateQuery(prologue + query + s)){
					rsclist.add(convertQuery(prologue + query + s));
					query = "";
					continue;
				}
				query += s + '\n';
			}
			if(query.length() > 0)
				rsclist.add(convertQuery(prologue + query));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}
		if(rsclist.size() > 1){
			Model md = ModelFactory.createDefaultModel();
			Resource cur = md.createResource("http://uri.sparqltest.org/ns#root");
			Resource last = null;
			for(int i = 0; i < rsclist.size(); i++){
				Resource newanon = md.createResource(new AnonId("_DBCLS_ANON_" + Integer.toString(i)));
				md.add(md.createStatement(cur, RDF.first, rsclist.get(i)));
				md.add(md.createStatement(cur, RDF.rest, newanon));
				last = cur;
				cur = newanon;
			}
			md.remove(last, RDF.rest, cur);
			md.add(md.createStatement(last, RDF.rest, RDF.nil));
			StringWriter sw = new StringWriter();
			md.write(sw, FileUtils.langNTriple);
			md.close();
			for(String t: sw.toString().split("\\n")){
				System.out.println(t.replaceFirst("\\.$", "") + graphname + " .");
			}
		}
	}
}
