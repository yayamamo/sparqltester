package jp.ac.rois.dbcls;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import org.apache.commons.lang3.time.FastDateFormat;
import org.topbraid.spin.arq.ARQFactory;
import org.topbraid.spin.model.*;
import org.topbraid.spin.system.SPINModuleRegistry;
import org.topbraid.spin.vocabulary.SP;
import org.apache.jena.atlas.io.IndentedWriter;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.XSD;
//import com.hp.hpl.jena.sparql.modify.UpdateProcessRemote;
import com.hp.hpl.jena.sparql.modify.UpdateProcessRemoteForm;
import com.hp.hpl.jena.update.UpdateRequest;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryParseException;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

/**
 * @author yayamamo (Yasunori Yamamoto, Database Center for Life Science)
 * Issues a list of SPARQL queries to asses the availability of a given SPARQL endpoint.
 * Special acknowledgments to Ryo Matsumiya 
 * License: MIT
 */

public class SPARQLTester {
	static String queryNumGraphs = "SELECT DISTINCT ?g WHERE { GRAPH ?g {?s ?p ?o} }";
	static HashSet<Resource> sQuery = new HashSet<Resource>(Arrays.asList(
			SP.Ask, SP.Construct, SP.Describe, SP.Select
			));
	static HashSet<Resource> sUpdate = new HashSet<Resource>(Arrays.asList(
			SP.Clear, SP.Create, SP.DeleteData, SP.DeleteWhere, SP.Drop, SP.InsertData, SP.Load, SP.Modify
			));
	static HashSet<Resource> stopSPIN = new HashSet<Resource>(Arrays.asList(
			SP.all, SP.arg, SP.arg1, SP.arg2, SP.arg3, SP.arg4, SP.arg5, SP.elements, SP.object, SP.path, SP.path1, SP.path2, SP.predicate,
			SP.resultNodes, SP.resultVariables, SP.serviceURI, SP.subject, SP.templates, SP.text, SP.varName, SP.varNames, SP.where 
			));
	static String queryEndpointUrl;
	static String impleName;
	static String impleVer;
	static String appliedEndpointUrl;
	static String appliedUpdateEndpointUrl;
	static String prefix = "http://uri.sparqltest.org/ns#";

	static private boolean issueSparqlQuery(String[] em, String query, Resource qt){
		QueryExecution gqexec = null;
		try {
			gqexec = QueryExecutionFactory.sparqlService(appliedEndpointUrl, query);
		} catch (QueryParseException e){
			System.out.println("Parse error!");
			System.out.println("## Query ##\n" + query + "###");
			em[0] = "Parse error.";
			return false;
		}
		gqexec.setTimeout(0, null, 0, null);
		ResultSet rs = null;
		Boolean ask = false;
		Model rsmodel = ModelFactory.createDefaultModel();
		ArrayList<String> sl = new ArrayList<String>();
		try {
			if(qt.getURI().contentEquals(SP.Select.toString())){
				rs = gqexec.execSelect();
				for(;rs.hasNext();){
					sl.add(rs.next().toString());
				}
			} else if(qt.getURI().contentEquals(SP.Construct.toString())){
				rsmodel = gqexec.execConstruct();
				rsmodel.toString();
			} else if(qt.getURI().contentEquals(SP.Describe.toString())){
				rsmodel = gqexec.execDescribe();
				rsmodel.toString();
			} else if(qt.getURI().contentEquals(SP.Ask.toString())){
				ask = gqexec.execAsk();
				ask.toString();
			}
			System.out.println("Ok.");
			return true;
		} catch (Exception e) {
			System.out.println(
					"Error: " + e.getMessage() + "\t" +
							"Cause: " + (e.getCause() == null?e.getCause():"") + "\t" +
							"Exception: " + e.toString());
			em[0] = e.getMessage();
			return false;
		} finally {
			gqexec.close();
		}
	}

	static private boolean issueUpdateQuery(String[] em, String query){
		UpdateRequest ur = new UpdateRequest();
		ur.add(query);
		try {
			UpdateProcessRemoteForm upr = new UpdateProcessRemoteForm(ur, appliedUpdateEndpointUrl, null);
			upr.execute();
			System.out.println("Update Ok.");
			return true;
		} catch (QueryParseException e){
			System.out.println("Parse error!");
			System.out.println("## Query ##\n" + query + "###");
			em[0] = "Parse error.";
			return false;
		} catch (Exception e){
			System.out.println(
					"Error: " + e.getMessage() + "\t" +
							"Cause: " + (e.getCause() == null?e.getCause():"") + "\t" +
							"Exception: " + e.toString());
			em[0] = e.getMessage();
			return false;
		}
	}

	static private boolean _graphApply(String[] em, Model model){
		return _graphApply(em, model, null);
	}

	static private boolean _graphApply(String[] em, Model model, Resource sbj){
		Property sp_text = model.createProperty("http://spinrdf.org/sp#text");
    	StmtIterator graphIterator = model.listStatements(sbj, RDF.type, (Resource)null);
    	boolean anyStatement = false;
    	while(graphIterator.hasNext()){
    		anyStatement = true;
    		Statement stmt = graphIterator.next();
    		Resource qt = stmt.getObject().asResource();
			String query = "";
			OutputStream baos = new ByteArrayOutputStream();
	    	IndentedWriter baos_indented = new IndentedWriter(baos);
    		if(sQuery.contains(qt)){
    			Resource rsrc = stmt.getSubject();
    			Statement query_text = model.getProperty(rsrc, sp_text);
    			if(query_text != null){
    				query = query_text.getLiteral().getString();
    			}else{
    				try {
    					org.topbraid.spin.model.Query queryObj = SPINFactory.asQuery(rsrc);
    					ARQFactory.get().createQuery(queryObj).output(baos_indented);
    					query = baos.toString();
    				} catch (QueryParseException e){
        				System.out.println("Parse Error.");
        				em[0] = "Parse Error.";
        				return false;
    				}
    			}
    			return issueSparqlQuery(em, query, qt);
    		} else if(sUpdate.contains(qt)){
    			Resource rsrc = stmt.getSubject();
    			Statement query_text = model.getProperty(rsrc, sp_text);
    			if(query_text != null){
    				query = query_text.getLiteral().getString();
    			}else{
    				try {
    					org.topbraid.spin.model.update.Update updateObj = SPINFactory.asUpdate(rsrc);
    					ARQFactory.get().createUpdateRequest(updateObj).output(baos_indented);
    					query = baos.toString();
    				} catch (QueryParseException e){
    					System.out.println("Parse Error.");
    					em[0] = "Parse Error.";
    					return false;
    				}
    			}
    			return issueUpdateQuery(em, query);
    		}
    	}
    	if(!anyStatement){
    		Statement query_text = model.getProperty(null, sp_text);
    		System.out.println("Query not issued.");
    		System.out.println("## Query ##\n" + query_text.getLiteral().getString() + "###");
    		em[0] = "Query not issued.";
    	}
    	return false;
	}

	static boolean graphApply(String graphUri, Model om, Resource root) {
		SPINModuleRegistry.get().init();
		Model model = ModelFactory.createDefaultModel();
		Resource thisGraph = om.createResource();
		root.addProperty(om.createProperty(prefix + "sparqlTestResult"), thisGraph);
		Resource properties = om.createResource();
		thisGraph.addProperty(RDF.type, om.createResource(prefix + "SparqlTestResult"));
		properties.addProperty(RDF.type, om.createResource(prefix + "QueryPropertySet"));
		thisGraph.addProperty(om.createProperty(prefix + "queryGraph"), om.createResource(graphUri));
		thisGraph.addProperty(om.createProperty(prefix + "queryProperty"), properties);
    	Property property = om.createProperty(prefix + "property");

    	String cqs = "CONSTRUCT {?s ?p ?o} {GRAPH <" + graphUri + "> {?s ?p ?o}}";
    	try {
    		Query cq = QueryFactory.create(cqs);
    		QueryExecution cqexec = QueryExecutionFactory.sparqlService(queryEndpointUrl, cq);
    		cqexec.execConstruct(model);
    	} catch (Exception e){
    		System.out.println("Error occurred during obtaining query data: " + graphUri);
    		System.out.println(e.toString());
    	}

		StmtIterator queryContIterator = model.listStatements(null, RDF.type, (Resource)null);
		HashSet<RDFNode> spinTypes = new HashSet<RDFNode>();
		for(;queryContIterator.hasNext();){
			RDFNode co = queryContIterator.next().getObject();
			spinTypes.add(co);
		}
		queryContIterator = model.listStatements(); 
		for(;queryContIterator.hasNext();){
			RDFNode p = queryContIterator.next().getPredicate();
			String kw = p.toString();
			if(!kw.startsWith(SP.NS))
				continue;
			if(stopSPIN.contains(p))
				continue;
			spinTypes.add(p);
		}
		for(RDFNode n: spinTypes){
			properties.addProperty(property, n);
		}
		System.out.print(graphUri + "\t"
				+ spinTypes.toString().replace(SP.NS,"sp:").replace(XSD.getURI(),"xsd:").replace("http://jena.hpl.hp.com/ARQ/function#","ARQ:")
				+ "\t");

		Date start = Calendar.getInstance().getTime();
		String query = "";
		String [] errMessage = new String[1];
		errMessage[0] = "";
		Resource cur = model.createResource("http://uri.sparqltest.org/ns#root");
		int stmtCount = 0;
		boolean result = true;
		while(model.contains(cur, RDF.rest)){
			stmtCount++;
			Resource rs = model.getProperty(cur, RDF.first).getResource();
			Resource rest = model.getProperty(cur, RDF.rest).getResource();
			if(stmtCount > 1)
				System.out.print(graphUri + "\t" + "\t");
			result &= _graphApply(errMessage, model, rs);
	    	StmtIterator textIterator = model.listStatements(rs, SP.text, (Resource)null);
	    	if(textIterator.hasNext())
	    		query += textIterator.next().getLiteral().getString();
			cur = rest;
		}
		if(stmtCount == 0){
			result = _graphApply(errMessage, model);
			NodeIterator ni = model.listObjectsOfProperty(SP.text);
			if(ni.hasNext())
				query += ni.next().toString();
		}
		thisGraph.addProperty(om.createProperty(prefix  + "success"), om.createTypedLiteral(result));
		thisGraph.addLiteral(om.createProperty(prefix + "message"), om.createTypedLiteral(errMessage[0]));
		Date end = Calendar.getInstance().getTime();
		FastDateFormat fdfIso8601ExtendedFormat = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ssZZ");
		thisGraph.addProperty(om.createProperty(prefix + "query"), query);
		thisGraph.addProperty(om.createProperty(prefix + "queryStartTime"), om.createTypedLiteral(fdfIso8601ExtendedFormat.format(start), XSDDatatype.XSDdateTime));
		thisGraph.addProperty(om.createProperty(prefix + "queryEndTime"), om.createTypedLiteral(fdfIso8601ExtendedFormat.format(end), XSDDatatype.XSDdateTime));
		return true;
	}

    public static void main(String[] args) throws Exception {
    	queryEndpointUrl = args[0];
    	impleName = args[1];
    	impleVer  = args[2];
    	appliedEndpointUrl = args[3];
    	if(args.length < 4)
    		System.out.println("Needs at least four arguments: <test sparql query endpoint> <implementation name> <implementation version> <target endpoint> [<target endpoint for update>]");
    	if(args.length == 5)
    		appliedUpdateEndpointUrl = args[4];
    	else
    		appliedUpdateEndpointUrl = appliedEndpointUrl;

    	Query gq = QueryFactory.create(queryNumGraphs);
		QueryExecution gqexec = QueryExecutionFactory.sparqlService(queryEndpointUrl, gq);
		try {
			Model om = ModelFactory.createDefaultModel();
			Resource root = om.createResource();
			root.addProperty(om.createProperty(prefix + "implementationName"), om.createTypedLiteral(impleName));
			root.addProperty(om.createProperty(prefix + "implementationVersion"), om.createTypedLiteral(impleVer));
			root.addProperty(RDF.type, om.createResource(prefix + "SparqlTestResultSet"));
			ResultSet rs = gqexec.execSelect();
			for (; rs.hasNext() ;){
				QuerySolution sln = rs.nextSolution();
				String graphName = sln.get("g").toString();
				graphApply(graphName, om, root);
			}
			try {
				OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream("result.ttl"));
				om.write(out, "TTL");
				//om.write(System.out, "TTL");
			} catch (FileNotFoundException e) {
				System.err.println("File couldn't be written.");
			}
		} catch (Exception e) {
			System.out.println("Error occurred during obtaining graph data.");
			System.out.println(e.toString());
		} finally { gqexec.close(); }
    }
}
