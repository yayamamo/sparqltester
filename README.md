# SPARQL Tester #

You can test the status of the SPARQL 1.1 support concerning a given SPARQL endpoint.

## Requirements ##
* [Apache Jena](http://jena.apache.org/)
* [Apache Commons](http://commons.apache.org/)
* [Java](http://www.oracle.com/technetwork/java/index.html)

## Compile ##
$ javac -cp commons-cli-1.2.jar:commons-lang3-3.3.2.jar:apache-jena-2.11.1/lib/*:./src ./src/jp/ac/rois/dbcls/SPARQLTester.java

## Execution ##
$ java -cp commons-cli-1.2.jar:commons-lang3-3.3.2.jar:apache-jena-2.11.1/lib/*:./src jp/ac/rois/dbcls/SPARQLTester

## License ##
MIT

---
Yasunori Yamamoto (yayamamo)

[Database Center for Life Science](http://dbcls.rois.ac.jp/)
